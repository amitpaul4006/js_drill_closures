const counterFactory = () => {
    let counterNumber = 5;
    let counterObject = {};
    counterObject = {
        increement : () => {
            return ++counterNumber;
        },
        decreement : () => {
            return --counterNumber;
        }
    }
    return counterObject;
}

module.exports = counterFactory;