const counterFactory = require('../counterFactory');

const counterOfNumber = counterFactory();
console.log('The incremented number', counterOfNumber.increement());
console.log('The incremented number',counterOfNumber.increement());
console.log('The incremented number',counterOfNumber.increement());
console.log('The decremented number',counterOfNumber.decreement());
console.log('The incremented number',counterOfNumber.decreement());
