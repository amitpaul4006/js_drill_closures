const cacheFunction = require('../cacheFunction');

const callback = (n) => {
    // console.log("Invoking callback")
    return "Invoking callback with " + n;
}

const resultOfCacheFunction = cacheFunction(callback);
console.log(resultOfCacheFunction(4));
console.log(resultOfCacheFunction(5));
console.log(resultOfCacheFunction(4));