let limitFunctionCallCount = (callback,n) => {

    let count = 0;
    let callingFunction = () => {
        if(count <= n){
            count++;
            return callback()
        }
        else{
            return null;
        }
    }
    return callingFunction;
}

module.exports = limitFunctionCallCount;