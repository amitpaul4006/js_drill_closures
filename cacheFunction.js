const cacheFunction = (callback) => {
    let cacheObject = {}
    const callingFunction = (n) => {
        if(cacheObject[n]) return cacheObject[n];
        else{
            const returnedVal = callback(n);
            cacheObject[n] = returnedVal;
            return cacheObject[n];
        }
        
    }
    return callingFunction;
}

module.exports = cacheFunction;